const mongoose = require("mongoose");

const commentsSchema = new mongoose.Schema({
  content: { type: String, required: true },
  user: { type: String },
});

const postSchema = new mongoose.Schema(
  {
    content: {
      type: String,
      required: true,
    },
    img: { type: String },
    likes: [String],
    comments: [
      {
        content: String,
        user: String,
      },
    ],
    tags: [String],
    user: String,
  },
  { timestamps: true }
);

postSchema.set("toJSON", {
  transform: (doc, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject._v;
  },
});

module.exports = mongoose.model("Post", postSchema);
