import React, { useEffect } from "react";
import { Grommet, Box, Button, Spinner, Header, Menu } from "grommet";
import * as Icons from "grommet-icons";
import { Route, withRouter, Switch } from "react-router-dom";

import Login from "./features/session/Login";
import Posts from "./features/posts/Posts";
import store from "./app/store";

import fire from "./fire";

import connect from "./socket-api";
import EditProfile from "./features/editprofile/EditProfile";
import UserProfile from "./features/editprofile/UserProfile";
import ListingImages from "./features/editprofile/ListingImages";
import CreatePost from "./features/posts/CreatePost";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style.css";

connect("http://localhost:3001", store);

const theme = {
  global: {
    focus: {
      border: {
        color: "transparent",
      },
    },
    colors: {
      brand: "#cc0000",
      back: "#292929",
      card: "#bfdbf7",
      accent: "#994650",
      ok: "#00C781",
    },
    font: {
      family: "Roboto",
      size: "18px",
      height: "20px",
    },
  },
};

function App({ history }) {
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [currentUser, setCurrentUser] = React.useState({});

  useEffect(() => {
    fire.auth().onAuthStateChanged((user) => {
      user ? setIsLoggedIn(true) : setIsLoggedIn(false);
      setLoading(false);
      if (user) setCurrentUser(user);
    });
  }, []);
  if (loading)
    return (
      <Box
        width="large"
        height="large"
        margin="auto"
        justify="center"
        align="center"
      >
        <Spinner />
      </Box>
    );
  return (
    <Grommet theme={theme} full>
      {isLoggedIn ? (
        <Header background="brand">
          <Button
            onClick={() => history.push("/")}
            icon={<Icons.Home />}
            hoverIndicator
          />
          <Menu
            label="Account"
            items={[
              {
                label: "Profile",
                onClick: () => {
                  history.push("/user-profile");
                },
              },
              {
                label: "Logout",
                onClick: () => {
                  fire.auth().signOut();
                  history.push("/user-profile");
                },
              },
            ]}
          />
        </Header>
      ) : (
        ""
      )}
      <Switch>
        {!isLoggedIn ? (
          <Route path="/">
            <Login />
          </Route>
        ) : (
          // <Box
          //   fill
          //   align="center"
          //   justify="center"
          //   background="back"
          //   overflow="auto"
          // >
          //   <Posts />
          //   <Button
          //     label="sign out"
          //     onClick={() => fire.auth().signOut()}
          //     margin={{ bottom: "medium" }}
          //   />
          // </Box>
          <>
            <Route path="/" exact>
              <Posts />
            </Route>
            <Route path="/edit-profile">
              <EditProfile />
            </Route>
            <Route path="/user-profile">
              <UserProfile user={currentUser} />
            </Route>
            <Route path="/listing">
              <Posts />
            </Route>
            <Route path="/createPost">
              <CreatePost />
            </Route>
          </>
        )}
      </Switch>
    </Grommet>
  );
}

export default withRouter(App);
