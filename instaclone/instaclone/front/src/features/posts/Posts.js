import React from "react";

import { Box, Button, Image, Text, TextInput } from "grommet";
import { Favorite } from "grommet-icons";

import Card from "../../components/Card";
import CardConcave from "../../components/CardConcave";
import Comment from "./Comment";

import { Scrollbars } from "react-custom-scrollbars";

import {
  addPost,
  getPosts,
  likePost,
  unlikePost,
} from "../../services/postsServices";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { update, selectPosts } from "./postsSlice";
import fire from "../../fire";
import { Badge } from "react-bootstrap";
const Posts = () => {
  const posts = useSelector(selectPosts);
  console.log("here it comes to listing posts", posts);
  const dispatch = useDispatch();

  const [refresh, setrefresh] = React.useState(true);
  const [showComment, setShowComment] = React.useState(null);
  const like = (post) => {
    likePost(post).then(() => setrefresh(true));
  };
  const unlike = (post) => {
    unlikePost(post).then(() => setrefresh(true));
  };

  React.useEffect(() => {
    const fecthPosts = async () => {
      const fetchData = await getPosts();
      dispatch(update(fetchData));
    };
    // if (refresh) {
    //   fecthPosts();
    //   setrefresh(false);
    // }
    fecthPosts();
  }, []);

  return (
    <Box align="center" margin="medium">
      <Card
        round="medium"
        padding="medium"
        justify="center"
        align="center"
        margin="medium"
        width="large"
        fill="vertical"
      >
        {posts && posts.length ? (
          <Button>
            <Link to="/createPost">Create New</Link>
          </Button>
        ) : (
          ""
        )}
        <Scrollbars autoHeight autoHeightMax="100%">
          {posts ? (
            !posts.length ? (
              <Text>
                Posts Not Found OR Error comes in fetching posts.{" "}
                {<Link to="/createPost">Click Here</Link>} to create new post
              </Text>
            ) : (
              posts.map((post, index) => (
                <CardConcave
                  round="small"
                  padding="medium"
                  margin={{ vertical: "small", horizontal: "large" }}
                  alignSelf="center"
                >
                  <Box margin={{ horizontal: "small", top: "small" }}>
                    <Text size="xsmall" weight="bold" color="white">
                      {"@" + post.user.split("@")[0]}
                    </Text>
                  </Box>
                  <Box
                    margin={{ horizontal: "small", bottom: "small" }}
                    direction="column"
                    justify="between"
                    align="center"
                  >
                    <Box fill="horizontal" gap="small">
                      <Text color="white">{post.content}</Text>
                      <Text color="white">
                        {post.tags
                          ? post.tags.map((tag) => (
                              <Badge
                                variant="secondary"
                                style={{ marginLeft: 5 }}
                              >
                                {tag}
                              </Badge>
                            ))
                          : ""}
                      </Text>
                      <Box fill="horizontal" align="center">
                        {post.img && (
                          <Image
                            src={`${post.img}`}
                            fit="cover"
                            style={{ maxWidth: 400, maxHeight: 400 }}
                          />
                        )}
                      </Box>
                    </Box>

                    <Box
                      direction="row"
                      justify="center"
                      align="center"
                      gap="small"
                    >
                      {post.likes.find(
                        (item) => item === fire.auth().currentUser.email
                      ) ? (
                        <Button
                          icon={<Favorite size="small" color="red" />}
                          onClick={() => unlike(post)}
                        />
                      ) : (
                        <Button
                          icon={<Favorite size="small" />}
                          onClick={() => like(post)}
                        />
                      )}

                      <Text size="small" color="white">
                        {post.likes?.length}
                      </Text>
                      <Text
                        size={"small"}
                        color="white"
                        style={{ cursor: "pointer" }}
                        onClick={() => setShowComment(index)}
                      >
                        Comment {post.comments?.length}
                      </Text>
                    </Box>
                    {showComment === index ? (
                      <Box
                        direction="column"
                        justify="center"
                        align="center"
                        gap="small"
                      >
                        {post.comments.map((comment) => {
                          return (
                            <>
                              <Box
                                margin={{ horizontal: "small", top: "small" }}
                              >
                                <Text color="white" size="xsmall" weight="bold">
                                  {comment.user.split("@")[0]}
                                </Text>
                              </Box>

                              <Box
                                margin={{
                                  horizontal: "small",
                                  bottom: "small",
                                }}
                                direction="column"
                                justify="between"
                                align="start"
                              >
                                <Box fill="horizontal" gap="small">
                                  <Text color="white">{comment.content}</Text>
                                </Box>
                              </Box>
                            </>
                          );
                        })}

                        <Box
                          direction="column"
                          justify="center"
                          align="center"
                          gap="small"
                        >
                          <Comment
                            post={post}
                            handleCancel={() => setShowComment(null)}
                          />
                        </Box>
                      </Box>
                    ) : (
                      ""
                    )}
                  </Box>
                </CardConcave>
              ))
            )
          ) : (
            <Text>Posts Not found</Text>
          )}
        </Scrollbars>
      </Card>
    </Box>
  );
};

export default Posts;
