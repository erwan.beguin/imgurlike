import {
  Box,
  FileInput,
  Form,
  FormField,
  TextInput,
  Button,
  Image,
} from "grommet";
import React, { useState } from "react";
import { addPost } from "../../services/postsServices";
import fire from "../../fire";
import { useHistory } from "react-router";

const CreatePost = () => {
  const [title, setTitle] = useState("");
  const [file, setFile] = useState("");
  const [tags, setTags] = useState("");
  const [image, setImage] = useState("");
  const history = useHistory();
  const handleSubmit = async () => {
    try {
      await addPost({ content: title, img: image, tags: tags.split(",") });
      history.push("/listing");
    } catch (err) {}
  };
  function uploadBlob(file) {
    return new Promise((fulfill, reject) => {
      const ref = fire
        .storage()
        .ref()
        .child("image-" + Date.now());

      // [START storage_upload_blob]
      // 'file' comes from the Blob or File API
      ref
        .put(file)
        .then((snapshot) => {
          fulfill(snapshot);
        })
        .catch((err) => reject(err));
      // [END storage_upload_blob]
    });
  }
  return (
    <Box title="Create Post" width="medium" height="medium" margin="auto">
      <h2>Create Post</h2>
      <Form onSubmit={handleSubmit}>
        <FormField
          label="Title"
          component={TextInput}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <FormField
          label="Tags (tags should be commad seperated eg. abc,efg"
          component={TextInput}
          value={tags}
          onChange={(e) => setTags(e.target.value)}
        />
        <FormField
          label="Upload Image"
          component={FileInput}
          value={file}
          onChange={async (e) => {
            if (!e.target.files.length) return;
            const file = e.target.files[0];
            const snap = await uploadBlob(file);
            const downloadUrl = await snap.ref.getDownloadURL();
            setImage(downloadUrl);
          }}
        />
        {image && (
          <Image
            src={`${image}`}
            fit="cover"
            style={{ maxWidth: 400, maxHeight: 400 }}
          />
        )}
        <Button
          style={{ marginTop: 15 }}
          label="Create"
          onClick={handleSubmit}
        ></Button>
      </Form>
    </Box>
  );
};

export default CreatePost;
