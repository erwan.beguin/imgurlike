import React from "react";
//import { connect } from 'react-redux';

class ListingImages extends React.Component {
  render() {
    return (
      <div>
        <div className="home">
          <h1>Home</h1>
        </div>
        <div className="container-fluid main-div">
          <img className="img-listing" src={require("../images/pp.jpg")} />
        </div>
        <div>
          <h1 className="text">Just a sample text</h1>
          <span className="text posted-by">
            Posted by: <span className="user-listing-name">@Pseudo</span>
            <div>
              <span className="tags">tags</span>
            </div>
          </span>
        </div>
      </div>
    );
  }
}
export default ListingImages;
