import React from "react";
import "./style.css";

import { Box, Text, TextInput, Button, Form, FormField, Inpu } from "grommet";

import fire from "../../fire";
import { Link } from "react-router-dom";

const Login = () => {
  const [email, setEmail] = React.useState();
  const [password, setPassword] = React.useState();
  const [alert, setalert] = React.useState();

  const handleSubmit = (e) => {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(email, password)
      .catch((error) => {
        setalert("Incorrect username or password");
      });
  };
  return (
    <Box pad="medium" width="medium" height="medium" margin="auto">
      <Form onSubmit={handleSubmit}>
        <h1 style={{ color: "white" }}>Login</h1>
        <FormField
          component={TextInput}
          type="text"
          id="email"
          //   class="fadeIn second"
          name="email"
          label="Email"
          placeholder="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <FormField
          type="password"
          id="password"
          //   class="fadeIn third"
          name="password"
          label="Password"
          placeholder="password"
          component={TextInput}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button
          type="submit"
          //   class="fadeIn fourth"
          label="Login"
          onClick={handleSubmit}
        />
      </Form>
    </Box>
  );
};

export default Login;

{
  /*
return ( <
        Box fill align = "center"
        justify = "center"
        background = "back"
        gap = "medium" >
        Login <
        Box width = "small"
        gap = "small" >
        <
        TextInput type = "email"
        placeholder = "email"
        onChange = {
            (e) => setEmail(e.target.value) }
        /> <
        TextInput type = "password"
        placeholder = "password"
        onChange = {
            (e) => setPassword(e.target.value) }
        /> <
        /Box>


        {
            alert && ( <
                Text color = "red" > { alert } < /Text>
            )
        }

        <
        Button margin = "small"
        label = "ok"
        onClick = {
            (e) => handleSubmit(e) }
        /> <
        /Box></Text>*/
}
