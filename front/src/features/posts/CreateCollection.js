import {
  Box,
  FileInput,
  Form,
  FormField,
  TextInput,
  Button,
  Image,
  Select,
} from "grommet";
import React, { useEffect, useState } from "react";
import {
  getPosts,
  addCollection,
  updateCollection,
  getSingleCollection,
} from "../../services/postsServices";
import fire from "../../fire";
import { useHistory, useParams } from "react-router";

const CreateCollection = () => {
  const [title, setTitle] = useState("");
  const [posts, setPosts] = useState([]);
  const params = useParams();
  // const [selectedPosts, setSelectedPosts] = useState([]);
  // useEffect(() => {
  //   (async () => {
  //     const list = await getPosts();
  //     setPosts(list);
  //   })();
  // }, []);
  // console.log({ selectedPosts });
  const history = useHistory();
  const handleSubmit = async () => {
    try {
      if (params.id) {
        await updateCollection({
          title,
          collectionId: params.id,
        });
      } else {
        await addCollection({
          title,
          posts: [],
        });
      }

      history.push("/user-profile");
    } catch (err) {}
  };
  useEffect(() => {
    if (params.id) {
      (async () => {
        const post = await getSingleCollection(params.id);
        const { title } = post;
        setTitle(title);
      })();
    }
  }, []);
  return (
    <Box title="Create Collection" width="medium" height="medium" margin="auto">
      <h2>Create Collection</h2>
      <Form onSubmit={handleSubmit}>
        <FormField
          label="Title"
          component={TextInput}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        {/* <FormField
          options={posts.map((item) => item.id)}
          label="Add Posts"
          component={Select}
          multiple
          // value={selectedPosts}
          onChange={(event) => {
            console.log({ event });
            // setSelectedPosts(selectedPosts.concat(option));
          }}
        /> */}
        {/* <FormField
          label="Tags (tags should be commad seperated eg. abc,efg"
          component={TextInput}
          value={tags}
          onChange={(e) => setTags(e.target.value)}
        /> */}
        {/* <FormField
          label="Upload Image"
          component={FileInput}
          value={file}
          onChange={async (e) => {
            if (!e.target.files.length) return;
            const file = e.target.files[0];
            const snap = await uploadBlob(file);
            const downloadUrl = await snap.ref.getDownloadURL();
            setImage(downloadUrl);
          }}
        />
        {image && (
          <Image
            src={`${image}`}
            fit="cover"
            style={{ maxWidth: 400, maxHeight: 400 }}
          />
        )} */}
        <Button
          style={{ marginTop: 15 }}
          label="Create"
          onClick={handleSubmit}
        ></Button>
      </Form>
    </Box>
  );
};

export default CreateCollection;
