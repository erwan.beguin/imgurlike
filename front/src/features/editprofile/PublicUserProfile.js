import React, { useEffect, useState } from "react";
import { Favorite } from "grommet-icons";
import {
  Row,
  Col,
  Card,
  Button,
  Badge,
  Container,
  Tabs,
  Tab,
} from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import fire from "../../fire";
import { useDispatch, useSelector } from "react-redux";
import { getPostsByUesr, getPostsByUserId } from "../../services/postsServices";
import { update } from "../posts/postsSlice";
import { Avatar } from "grommet";

const PublicUserProfile = (props) => {
  let user = props.user || {};
  const [key, setKey] = useState("collections");
  const history = useHistory();
  const dispatch = useDispatch();
  console.log({ user });
  const posts = useSelector((state) => state.posts.value);
  useEffect(() => {
    (async () => {
      const fetchdPosts = await getPostsByUserId(user.email);
      dispatch(update(fetchdPosts));
    })();
  }, []);
  let name,
    handle = "";
  if (user && user.displayName) [name, handle] = user.displayName?.split(",");
  return (
    <Container>
      <Row>
        <h2>User Profile</h2>
      </Row>
      <Row>
        <Col md={5} lg={5}>
          <Row>
            <Col md={2} lg={2}>
              <Avatar src={user.photoURL} />
            </Col>
            <Col md={8} lg={8} as="p" style={{ marginTop: 7, marginRight: 20 }}>
              {name || ""}
              <br />
              {handle && (
                <Link to={`profile/${user.email}`}>{"@" + handle}</Link>
              )}
            </Col>
          </Row>
        </Col>
        {/* <Col /> */}
        <Col></Col>
        <Col>
          <Button onClick={() => history.push("/createPost")}>
            Create New Post
          </Button>
          <Button
            variant="secondary"
            onClick={() => history.push("/edit-profile")}
            style={{ marginLeft: 7 }}
          >
            Edit Profile
          </Button>
        </Col>{" "}
      </Row>
      <Row style={{ marginTop: 25 }}>
        <Col>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
          >
            <Tab eventKey="collections" title="Collections">
              <Row>
                {posts.map((post) => {
                  return (
                    <Col
                      md={3}
                      lg={3}
                      sm={12}
                      style={{ marginTop: 5, color: "black" }}
                    >
                      <Card>
                        <Card.Img variant="top" src={post.img} />
                        <Card.Body>
                          <Card.Title>{post.user.split("@")[0]}</Card.Title>
                          <Card.Text>
                            <div>
                              {post.content}
                              <div>
                                {post.tags?.map((tag) => (
                                  <Badge
                                    variant="secondary"
                                    style={{ marginLeft: 5 }}
                                  >
                                    {tag}
                                  </Badge>
                                ))}
                              </div>
                            </div>
                          </Card.Text>
                          {/* <Button variant="primary">Go somewhere</Button> */}
                        </Card.Body>
                      </Card>
                    </Col>
                  );
                })}
              </Row>
            </Tab>
          </Tabs>
        </Col>
      </Row>
    </Container>
  );
};

export default PublicUserProfile;
