import React from "react";
import { Route, withRouter } from "react-router-dom";
import fire from "./fire";

class ProtectedRoute extends React.Component {
  state = {
    auth: false,
  };
  async componentDidMount() {
    try {
      const user = fire.auth().currentUser;
      if (!user) throw new Error("user not found");
    } catch (err) {
      fire.auth().signOut();
    }
  }

  render() {
    const { children, ...rest } = this.props;
    return this.state.auth ? (
      <Route {...rest}>{children}</Route>
    ) : (
      <div
        style={{
          width: "100%",
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        Loading...
      </div>
    );
  }
}

export default withRouter(ProtectedRoute);
