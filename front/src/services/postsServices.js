import axios from "axios";

import fire from "../fire";

const url = "http://localhost:3001/posts";
const collectionUrl = "http://localhost:3001/collections";

const createToken = async () => {
  const user = fire.auth().currentUser;
  const token = user && (await user.getIdToken());

  const payloadHeader = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  return payloadHeader;
};

export const addPost = async (formData) => {
  const header = await createToken();
  try {
    const res = await axios.post(url, formData, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const deletePost = async (formData) => {
  const header = await createToken();
  try {
    const res = await axios.post(url + "/deletePost", formData, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const updatePost = async (formData) => {
  const header = await createToken();
  try {
    const res = await axios.post(url + "/updatePost", formData, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getSinglePost = async (id) => {
  const header = await createToken();
  try {
    const res = await axios.get(url + "/singlePost/" + id, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getSingleCollection = async (id) => {
  const header = await createToken();
  try {
    const res = await axios.post(
      collectionUrl + "/getSingleCollection",
      { collectionId: id },
      header
    );
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const addCollection = async (formData) => {
  const header = await createToken();
  try {
    const res = await axios.post(collectionUrl + "/create", formData, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const deleteCollection = async (formData) => {
  const header = await createToken();
  try {
    const res = await axios.post(
      collectionUrl + "/deleteCollection",
      formData,
      header
    );
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const updateCollection = async (formData) => {
  const header = await createToken();
  try {
    const res = await axios.post(
      collectionUrl + "/updateCollection",
      formData,
      header
    );
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getPosts = async () => {
  const header = await createToken();

  try {
    const res = await axios.get(url, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getCollections = async () => {
  const header = await createToken();

  try {
    const res = await axios.get(collectionUrl, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getPostsByUesr = async () => {
  const header = await createToken();

  try {
    const res = await axios.get(url + "/postsByUser", header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getCollectionsByUesr = async () => {
  const header = await createToken();

  try {
    const res = await axios.get(collectionUrl + "/collectionsByUser", header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getPostsByLikes = async () => {
  const header = await createToken();

  try {
    const res = await axios.get(url + "/postsByLike", header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const getPostsByUserId = async (userId) => {
  const header = await createToken();

  try {
    const res = await axios.get(url + "/postsByUser/" + userId, header);
    return res.data;
  } catch (e) {
    console.error(e);
    return [];
  }
};

export const getCollectionsByUserId = async (userId) => {
  const header = await createToken();

  try {
    const res = await axios.get(
      collectionUrl + "/collectionsByUser/" + userId,
      header
    );
    return res.data;
  } catch (e) {
    console.error(e);
    return [];
  }
};

export const addToCollection = async (collectionId, postId) => {
  const header = await createToken();

  try {
    const res = await axios.post(
      collectionUrl + "/addPostToCollection",
      { collectionId, postId },

      header
    );
    return res.data;
  } catch (e) {
    console.error(e);
    return [];
  }
};

export const removeFromCollection = async (collectionId, postId) => {
  const header = await createToken();

  try {
    const res = await axios.post(
      collectionUrl + "/removePostFromCollection",
      { collectionId, postId },

      header
    );
    return res.data;
  } catch (e) {
    console.error(e);
    return [];
  }
};

export const getUserById = async (userId) => {
  const header = await createToken();
  try {
    const res = await axios.get(url + "/userById/" + userId, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};
export const likePost = async (post) => {
  const header = await createToken();

  const likes = [...post.likes];
  likes.push(fire.auth().currentUser.email);
  const data = {
    id: post.id,
    like: true,
  };
  try {
    const res = await axios.post(url + "/like", data, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const unlikePost = async (post) => {
  const header = await createToken();

  const likes = [...post.likes];

  const index = likes.indexOf(fire.auth().currentUser.email);
  if (index > -1) {
    likes.splice(index, 1);
  }
  const data = {
    id: post.id,
    like: false,
  };
  try {
    const res = await axios.post(url + "/like", data, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};

export const addComment = async (comment, postId) => {
  const header = await createToken();
  const data = {
    id: postId,
    content: comment,
  };
  try {
    const res = await axios.post(url + "/comment", data, header);
    return res.data;
  } catch (e) {
    console.error(e);
  }
};
